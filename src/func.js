const getSum = (str1, str2) => {
	if (
		typeof str1 === "object" ||
		typeof str2 === "object" ||
		isNaN(str1) ||
		isNaN(str2)
	) {
		return false;
	}
	str1 = str1.trim() === "" ? 0 : parseFloat(str1);
	str2 = str2.trim() === "" ? 0 : parseFloat(str2);
	return `${str1 + str2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
	let posts = listOfPosts.filter((post) => post.author === authorName).length;
	let comments = listOfPosts.reduce((quantityOfComments, post) => {
		return (
			quantityOfComments +
			(post.comments === undefined
				? 0
				: post.comments.filter((comment) => comment.author === authorName)
						.length)
		);
	}, 0);
	return `Post:${posts},comments:${comments}`;
};


const tickets = (people) => {
	const ticketPrice = 25;
	let totalSum = 0;
	for (let item of people) {
		if (totalSum < item - ticketPrice) {
			return "NO";
		} else {
			totalSum += ticketPrice;
		}
	}
	return "YES";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
